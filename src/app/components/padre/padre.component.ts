import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-padre',
  templateUrl: './padre.component.html',
  styleUrls: ['./padre.component.css']
})
export class PadreComponent implements OnInit {

  mensajePadre1="Barcelona"
  mensajePadre2="Real Madrid"

  Equipo1 = {
    nroCopas: 152,
    pais:'España',
    añosActivo: 70
  }

  Equipo2 = {
    nroCopas: 149,
    pais: 'España',
    añosActivo: 68
  }

  constructor() {
  
   }

  ngOnInit(): void {
  }

}
